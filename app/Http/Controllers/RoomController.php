<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rooms;

class RoomController extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request ){

        $category = $request->input('category', null);
        $model_rooms = new Rooms();

        $rooms = $model_rooms->getRoomsByCategory($category, 'persons');
        $categories = $model_rooms->getCategories();

        if($category){
            $category_info = $model_rooms->getCategoryById($category);
            $page_title = 'Номери категорії ' . $category_info->category_desc;
        } else {
            $page_title = 'Всі номери';
        }

        return view('app.room.index', [
            'rooms' => $rooms,
            'categories' => $categories,
            'category_selected' => $category,
            'title' => $page_title]);



        $category = $request->input('category', null);


        $title = !$category ? 'All Rooms' : 'Rooms of category: ' .
            $categories[$category]['category_desc'];

        if ($category){
                $rooms_query = array();

                foreach ($rooms as $room){
                    if ($room['category'] == $category) {
                        array_push($rooms_query, $room);
                    }
                }
                    return view('app.room.index')
                        ->with('rooms', $rooms_query)
                        ->with('title', $title)
                        ->with('category_selected', $category)
                        ->with('categories', $categories);
                

        }
        else {
            return view('app.room.index')
                ->with('rooms', $rooms)
                ->with('title', $title)
                ->with('category_selected', $category)
                ->with('categories', $categories);
        }
    }


    public function room($id){

            $model_rooms = new Rooms();

            $categories = $model_rooms->getCategories();
            $room = $model_rooms->getRoomById($id);

            return view('app.room.show')
                ->with('room', $room)
                ->with('categories', $categories);

    }


}
