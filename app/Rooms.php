<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Rooms
{
    /**
     * вибір кімнат за категорією
     * @param $category_id
     * @param $order_column
     * @return array
     */
    public function getRoomsByCategory($category_id, $order_column){
        if(!$category_id){
            $rooms = DB::select(
                'SELECT r.*, category_desc 
                          FROM rooms r
                          JOIN room_categories rc ON r.category_id = rc.id 
                      ORDER BY ' . $order_column);
        } else {
            $rooms = DB::select(
                'SELECT r.*, category_desc 
                         FROM rooms r
                         JOIN room_categories rc ON r.category_id = rc.id 
                        WHERE category_id = :category_id 
                     ORDER BY ' . $order_column, ['category_id' => $category_id]);
        }
        return $rooms;
    }


    /**
     * вибір кімнати по номеру
     * @param $room_id
     * @return mixed
     */
    public function getRoomById($room_id){
        $room = DB::table('rooms')
            ->where('room_id', $room_id)
            ->join('room_categories', 'rooms.category_id', '=', 'room_categories.id')
            ->get()
            ->first();

        return $room;
    }


    /**
     * вибір категорій
     */
    public function getCategories(){
        $categories = DB::select('SELECT * FROM room_categories ORDER BY id');
        return $categories;
    }


    /**
     * вибір категорії по ідентифікатору
     * @param $category_id
     * @return mixed
     */
    public function getCategoryById($category_id){
        //$category = DB::select('SELECT * FROM room_categories WHERE id = :cid ORDER BY id', ['cid' => $category_id]);
        //$category = $category[0];

        $category = DB::table('room_categories')->find($category_id);
        return $category;
    }


}
