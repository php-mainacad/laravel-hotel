<html>

<head>
    <title> Hotel - Laravel - Application </title>
</head>

<body>
    @section('page_title')
        <p>Name of site</p>
    @show

    <br/>
    <br/>

    <div class="container">
        @yield('nav')
        @yield('content')

    </div>
</body>

@include('app.layouts.footer')

</html>