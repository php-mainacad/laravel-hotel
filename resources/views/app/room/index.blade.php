@extends('app.layouts.layout')


@section('page_title')
    @parent
    <b>{{ $title }}</b>
@endsection

@section('nav')
    <form method="get" action="/rooms">
        <select name="category">
            <option value="0">всі категорії</option>

            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                        {{ ( $category->id == $category_selected ) ? 'selected' : '' }}>
                    {{ $category->category_desc }}
                </option>
            @endforeach
        </select>

        <input type="submit" value="Знайти" />
    </form>
@endsection

@section('content')
    <p>Перелік номерів.</p>

    <table>
        <th>Номер кімнати</th>
        <th>Кількість місць</th>
        <th>Клас</th>
        <th>Ціна</th>
        @foreach ($rooms as $room)
            <tr>
                <td>
                    <a href="/rooms/{{ $room->room_id }}">{{ $room->room_id }}</a>
                </td>
                <td>{{ $room->persons }}</td>
                <td>{{ $room->category_desc }}</td>
                <td>{{ $room->price }}</td>
            </tr>
        @endforeach
    </table>

@endsection