@extends('app.layouts.layout')

@section('page_title')
    <b>Інформація про номер {{ $room->room_id }}</b>
@endsection

@section('content')
    <p>Номер - {{ $room->room_id }}</p>
    <p>Кількість місць - {{ $room->persons }}</p>
    <p>Клас - {{ $room->category_desc }}</p>
    <p>Ціна - {{ $room->price }}</p>

    <a href="/rooms">Дивитися всі номери</a>
@endsection
